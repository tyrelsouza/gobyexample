package main

import "fmt"

func main() {
	messages := make(chan string, 2)

	messages <- "mess1"
	messages <- "mess2"

	fmt.Println(<-messages)
	fmt.Println(<-messages)
}
